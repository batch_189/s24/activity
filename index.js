

const getCube = (a) => a ** 3;

let num = 2
console.log(`The cube of ${num} is ${getCube(num)}`);


const address = ["Bagong Barrio", "Caloocan City", "Philippines"]
const [state, city, countries] = address
console.log(`I live at ${state}, ${city}, ${countries}!`);


const animal = {
    givenName: "crocodile",
    weight: 1075,
    measurement: 20,
    inches: 3,
};

console.log(`lolong was a saltwater ${animal.givenName}. He weighed at ${animal.weight} kgs with a measurement of ${animal.measurement} ft ${animal.inches} in.`)


const numbers = [1, 2, 3, 4, 5];
numbers.forEach(number => console.log(number));



class Dog {
    constructor(name, age, breed) {
        this.name = "Tash";
        this.age = 2;
        this.breed = "Mongrel";
    }
};

const myDog = new Dog ();
console.log(myDog);




